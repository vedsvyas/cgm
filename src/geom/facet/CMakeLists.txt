project(cgm_facet)

set(facet_srcs
  CubitOctree.cpp
  CubitOctreeCell.cpp
  CubitOctreeGenerator.cpp
  CubitOctreeGeneratorVolumes.cpp
  CubitOctreeNode.cpp
  FacetAttrib.cpp
  FacetAttribSet.cpp
  FacetBody.cpp
  FacetCoEdge.cpp
  FacetCurve.cpp
  #FacetGeometryCreator.cpp
  FacetLoop.cpp
  FacetLump.cpp
  FacetModifyEngine.cpp
  FacetParamTool.cpp
  FacetPoint.cpp
  FacetProjectTool.cpp
  FacetQueryEngine.cpp
  FacetShell.cpp
  FacetSurface.cpp
  FacetboolInterface.cpp
  GridSearchTree.cpp
  OctreeFacetPointData.cpp
  OctreeIntersectionData.cpp
  PST_Data.cpp
  TDOctreeRefEdge.cpp
  TDOctreeRefFace.cpp)

set(facet_headers
  cgm/CubitOctree.hpp
  cgm/CubitOctreeCell.hpp
  cgm/CubitOctreeConstants.hpp
  cgm/CubitOctreeGenerator.hpp
  cgm/CubitOctreeGeneratorVolumes.hpp
  cgm/CubitOctreeNode.hpp
  cgm/FacetAttrib.hpp
  cgm/FacetAttribSet.hpp
  cgm/FacetBody.hpp
  cgm/FacetCoEdge.hpp
  cgm/FacetCurve.hpp
  cgm/FacetGeometryCreator.hpp
  cgm/FacetLoop.hpp
  cgm/FacetLump.hpp
  cgm/FacetModifyEngine.hpp
  cgm/FacetParamTool.hpp
  cgm/FacetPoint.hpp
  cgm/FacetProjectTool.hpp
  cgm/FacetQueryEngine.hpp
  cgm/FacetShell.hpp
  cgm/FacetSurface.hpp
  cgm/FacetboolInterface.hpp
  cgm/GridSearchTree.hpp
  cgm/GridSearchTreeNode.hpp
  cgm/OctreeFacetPointData.hpp
  cgm/OctreeIntersectionData.hpp
  cgm/PST_Data.hpp
  cgm/TDOctreeRefEdge.hpp
  cgm/TDOctreeRefFace.hpp)

link_libraries(
    cgm_facetbool
    cgm_cholla
    cgm_geom
    cgm_util
)
cgm_add_library(cgm_facet 1 ${facet_srcs} ${facet_headers})

include_directories(cgm_facet
    ${CMAKE_CURRENT_SOURCE_DIR}/cgm
    ${CMAKE_SOURCE_DIR}/src/util/cgm
    ${CMAKE_BINARY_DIR}/src/util/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/cgm
    ${CMAKE_BINARY_DIR}/src/geom/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/Cholla/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/facetbool/cgm
)

install(
  TARGETS   "cgm_facet"
  EXPORT    cgm_geom
  RUNTIME   DESTINATION bin
  LIBRARY   DESTINATION lib
  ARCHIVE   DESTINATION lib
  COMPONENT Runtime)

cgm_install_headers(${facet_headers})


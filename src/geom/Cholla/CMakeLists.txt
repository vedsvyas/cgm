project(cgm_cholla)

include(CGMMacros)

set(cholla_srcs
  AllocMemManagersCholla.cpp
  Cholla.cpp
  ChollaCurve.cpp
  ChollaEngine.cpp
  ChollaEntity.cpp
  ChollaPoint.cpp
  ChollaSkinTool.cpp
  ChollaSurface.cpp
  ChollaVolume.cpp
  ChordalAxis.cpp
  CubitFacet.cpp
  CubitFacetData.cpp
  CubitFacetEdge.cpp
  CubitFacetEdgeData.cpp
  CubitPoint.cpp
  CubitPointData.cpp
  CubitQuadFacet.cpp
  CubitQuadFacetData.cpp
  CurveFacetEvalTool.cpp
  FacetDataUtil.cpp
  FacetEntity.cpp
  FacetEvalTool.cpp
  GeoNode.cpp
  GeoTet.cpp
  LoopParamTool.cpp
  PointGridSearch.cpp
  PointLoopFacetor.cpp
  TDChordal.cpp
  TDFacetBoundaryEdge.cpp
  TDFacetBoundaryPoint.cpp
  TDFacetboolData.cpp
  TDGeomFacet.cpp
  ChollaDebug.cpp)

set(cholla_headers
  cgm/BoundaryConstrainTool.cpp
  cgm/BoundaryConstrainTool.hpp
  cgm/Cholla.h
  cgm/ChollaCurve.hpp
  cgm/ChollaEngine.hpp
  cgm/ChollaEntity.hpp
  cgm/ChollaPoint.hpp
  cgm/ChollaSkinTool.hpp
  cgm/ChollaSurface.hpp
  cgm/ChollaVolume.hpp
  cgm/ChordalAxis.hpp
  cgm/CubitFacet.hpp
  cgm/CubitFacetData.hpp
  cgm/CubitFacetEdge.hpp
  cgm/CubitFacetEdgeData.hpp
  cgm/CubitPoint.hpp
  cgm/CubitPointData.hpp
  cgm/CubitQuadFacet.hpp
  cgm/CubitQuadFacetData.hpp
  cgm/CurveFacetEvalTool.hpp
  cgm/FacetDataUtil.hpp
  cgm/FacetEntity.hpp
  cgm/FacetEvalTool.hpp
  cgm/FacetorTool.cpp
  cgm/FacetorTool.hpp
  cgm/FacetorUtil.cpp
  cgm/FacetorUtil.hpp
  cgm/GeoNode.hpp
  cgm/GeoTet.hpp
  cgm/LoopParamTool.hpp
  cgm/PointGridSearch.hpp
  cgm/PointLoopFacetor.hpp
  cgm/TDChordal.hpp
  cgm/TDDelaunay.cpp
  cgm/TDDelaunay.hpp
  cgm/TDFacetBoundaryEdge.hpp
  cgm/TDFacetBoundaryPoint.hpp
  cgm/TDFacetboolData.hpp
  cgm/TDGeomFacet.hpp
  TDInterpNode.hpp
  cgm/TetFacetorTool.cpp
  cgm/TetFacetorTool.hpp
  cgm/ChollaDebug.hpp)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/cgm
    ${CMAKE_SOURCE_DIR}/src/util/cgm
    ${CMAKE_BINARY_DIR}/src/util/cgm
)

link_libraries(
    cgm_geom
    cgm_util
)
cgm_add_library(cgm_cholla 1 ${cholla_srcs})

install(
  TARGETS   "cgm_cholla"
  EXPORT    cgm_geom
  RUNTIME   DESTINATION bin
  LIBRARY   DESTINATION lib
  ARCHIVE   DESTINATION lib
  COMPONENT Runtime)

cgm_install_headers(${cholla_headers})

